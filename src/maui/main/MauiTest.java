package maui.main;

import java.util.Enumeration;
import java.util.HashSet;

import weka.core.Option;

public class MauiTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String option = args[0];
		String actionFolder = args[1];
		String model = args[2];
		String vocabularyOption = args[3];
		String configFile = args[4];
		
		if(option.equals("train"))
			train(actionFolder, model, vocabularyOption, configFile);
		else if(option.equals("test"))
			test(actionFolder, model, vocabularyOption, configFile);
		else
			System.out.println("Not a valid option ... ");

	}
	
	public static void train(String actionFolder, String model, String vocabularyOption, String configFile) {
		
		System.out.println("Training");
		
		MauiModelBuilder modelBuilder = new MauiModelBuilder();
		String [] ops = {	"-l", actionFolder,
							"-m", model,
							"-v", vocabularyOption, 
							"-c", configFile
				};
		
		try {
	
		    modelBuilder.setOptions(ops);
	
		    // Output what options are used
		    if (modelBuilder.getDebug() == true) {
			System.err.print("Building model with options: ");
			String[] optionSettings = modelBuilder.getOptions();
			for (String optionSetting : optionSettings) {
			    System.err.print(optionSetting + " ");
			}
			System.err.println();
		    }
	
		    HashSet<String> fileNames = modelBuilder.collectStems();
		    modelBuilder.buildModel(fileNames);
	
		    if (modelBuilder.getDebug() == true) {
			System.err.print("Model built. Saving the model...");
		    }
	
		    modelBuilder.saveModel();
	
		    System.err.print("Done!");
				
		} catch (Exception e) {
	
		    // Output information on how to use this class
		    e.printStackTrace();
		    System.err.println(e.getMessage());
		    System.err.println("\nOptions:\n");
		    Enumeration<Option> en = modelBuilder.listOptions();
		    while (en.hasMoreElements()) {
			Option option = (Option) en.nextElement();
			System.err.println(option.synopsis());
			System.err.println(option.description());
		    }
		}
	}
	
	public static void test(String actionFolder, String model, String vocabularyOption, String configFile) {
		
		System.out.println("Testing");
		
		MauiTopicExtractor topicExtractor = new MauiTopicExtractor();
		String [] ops = {	"-l", actionFolder,
							"-m", model,
							"-v", vocabularyOption, 
							"-c", configFile
		};
		
		try {
			// Checking and Setting Options selected by the user:
		    topicExtractor.setOptions(ops);      
		    
		    System.err.print("Extracting keyphrases with options: ");
		    
		    // Reading Options, which were set above and output them:
		    String[] optionSettings = topicExtractor.getOptions();
		    for (int i = 0; i < optionSettings.length; i++) {
			System.err.print(optionSettings[i] + " ");
		    }
		    System.err.println();
			
		    // Loading selected Model:
		    System.err.println("-- Loading the model... ");
		    topicExtractor.loadModel();
		    	
		    // Extracting Keyphrases from all files in the selected directory
		    topicExtractor.extractKeyphrases(topicExtractor.collectStems());
			
		} catch (Exception e) {
			
		    // Output information on how to use this class
		    e.printStackTrace();
		    System.err.println(e.getMessage());
		    System.err.println("\nOptions:\n");
		    Enumeration<Option> en = topicExtractor.listOptions();
		    while (en.hasMoreElements()) {
			Option option = (Option) en.nextElement();
			System.err.println(option.synopsis());
			System.err.println(option.description());
		    }
		}
	}

}
